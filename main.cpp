#include <iostream>
#include <fstream>
#include <string>
#include <utility>
#include <cstdlib>
#include <elnormous/HTTPRequest.hpp>
#include <nlohmann/json.hpp>

/**
 * Load environment variables from a .env file.
 *
 * @param filename The name of the .env file.
 * @return Boolean indicating success or failure.
 */
bool loadEnvFile(const std::string& filename) {
    std::ifstream file(filename);
    if (!file) {
        std::cerr << "Failed to open .env file: " << filename << std::endl;
        return false;
    }

    std::string line;
    while (std::getline(file, line)) {
        auto delimiterPos = line.find('=');
        std::string name = line.substr(0, delimiterPos);
        std::string value = line.substr(delimiterPos + 1);
        setenv(name.c_str(), value.c_str(), 1);
    }

    file.close();
    return true;
}

/**
 * Retrieves the API key from the config.txt file.
 *
 * @return The API key as a string. Returns an empty string if the key is not found.
 */
std::string getApiKey() {
    const char* envApiKey = std::getenv("API_KEY");
    if (envApiKey) {
        return std::string(envApiKey);
    } else {
        std::cerr << "API Key is not set in environment variables.\n";
        return "";
    }
}

/**
 * Performs an HTTP GET request to the specified URL.
 *
 * @param url The URL to request data from.
 * @return A string containing the response data.
 */
std::string httpRequest(const std::string& url) {
    try {
        http::Request request{url};

        // Send a get request
        const auto response = request.send("GET");
        return std::string{response.body.begin(), response.body.end()};
    }
    catch (const std::exception& e) {
        std::cerr << "Request failed, error: " << e.what() << std::endl;
        return "";
    }
}

/**
 * Geocodes a zipcode to retrieve its latitude and longitude coordinates using OpenWeatherMap's Geocoding API.
 *
 * @param zipCode A string representing the zipcode to geocode.
 * @param apiKey The API key for accessing the OpenWeatherMap API.
 * @return A pair containing the latitude and longitude of the geocoded location.
 */
std::pair<double, double> geocodeZipcode(const std::string& zipCode, const std::string& apiKey) {
    std::string url = "http://api.openweathermap.org/geo/1.0/zip?zip=" + zipCode + ",US&appid=" + apiKey;
    std::string response = httpRequest(url);
    auto json = nlohmann::json::parse(response);
    if (!json.empty()) {
        double lat = json["lat"];
        double lon = json["lon"];
        return {lat, lon};
    }
    return {0, 0}; // Return an invalid location if not found
}

/**
 * Fetches air quality data from the OpenWeatherMap Air Pollution API using geographic coordinates.
 *
 * @param lat The latitude of the location.
 * @param lon The longitude of the location.
 * @param apiKey The API key for accessing the OpenWeatherMap data.
 * @return A string containing the API response in JSON format.
 */
std::string fetchAirQuality(double lat, double lon, const std::string& apiKey) {
    std::string url = "http://api.openweathermap.org/data/2.5/air_pollution?lat=" + std::to_string(lat) + "&lon=" + std::to_string(lon) + "&appid=" + apiKey;
    return httpRequest(url);
}

/**
 * Main function. Reads the API key from a file, takes a zipcode as command line argument,
 * geocodes the zipcode, fetches the air quality data, and prints it.
 *
 * @param argc Argument count.
 * @param argv Argument vector; expects the zipcode as argv[1].
 * @return Integer exit code of the program.
 */
int main(int argc, char* argv[]) {

    if (!loadEnvFile(".env")) {
        return 1;
    }

    if (argc < 2) {
        std::cout << "Usage: " << argv[0] << " <Zip Code>" << std::endl;
        return 1;
    }
    std::string apiKey = getApiKey();
    if (apiKey.empty()) {
        return 1;
    }
    std::string zipCode = argv[1];
    auto [lat, lon] = geocodeZipcode(zipCode, apiKey);

    std::string response = fetchAirQuality(lat, lon, apiKey);
    try {
        auto json = nlohmann::json::parse(response);
        if (json.contains("list") && !json["list"].empty() && json["list"][0].contains("main")) {
            auto aqi = json["list"][0]["main"]["aqi"];
            std::cout << "Air Quality Index (AQI) for Zip Code " << zipCode << ": " << aqi << " (" << (aqi == 1 ? "Good" : aqi == 2 ? "Fair" : aqi == 3 ? "Moderate" : aqi == 4 ? "Poor" : "Very Poor") << ")" << std::endl;
        } else {
            std::cout << "No AQI data available for Zip Code " << zipCode << std::endl;
        }
    } catch (std::exception& e) {
        std::cerr << "Failed to parse JSON response: " << e.what() << std::endl;
    }

    return 0;
}
