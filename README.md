# Air Quality Monitoring Application

A C++ program for an air quality monitoring application used as a demo application for a blog post about unit testing. 

It: 
- geocodes locations from a location zipcode
- fetches air quality data based on the geolocation provided
- displays the AQI for the specified location

Code was generated using [GitLab Duo](https://about.gitlab.com/gitlab-duo/).

## Prerequisites

- C++ Compiler (e.g., GCC or Clang)
- libcurl (for making HTTP requests)
- [nlohmann/json](https://github.com/nlohmann/json/releases) (for JSON parsing)
- You can install these with brew `brew install curl nlohmann-json`

## Installation Steps

1. Clone the repository:

```bash
   git clone [repository-url]
   cd [repository-directory]
```

Add includes/ folder and nlohmann/json.hpp.

2. Acquire your OpenWeatherMap API key and create a `.env` file to store your key.

```
API_KEY="YOURAPIKEY_HERE"
```
3. Make sure to add `.env` to your `.gitignore` file
4. Compile and build the project

```
cmake -S . -B build
cmake --build build
```

4. Run the application (90210 as an example zipcode)

```
./air_quality_app 90210
```